package firstPackage;

public abstract class Poligono {
    private int altezza;
    private int base;
    private String tipo;
    private String name;

    public Poligono(int base, int altezza, String name){
        this.base = base;
        this.altezza = altezza;
        this.name = name;
    }

    public int getArea(){
        return base*altezza;
    }

    public int getAltezza() {
        return altezza;
    }

    public int getBase() {
        return base;
    }

    public String getName() {
        return name;
    }

    public void setTipo(String tipo){
        this.tipo = tipo;
    }

}
