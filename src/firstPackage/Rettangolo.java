package firstPackage;

public class Rettangolo extends Poligono{

    public Rettangolo(int base, int altezza, String nome) {
        super(base, altezza, nome);
        setTipo("Rettangolo");
    }
}
