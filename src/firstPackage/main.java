package firstPackage;

import java.util.ArrayList;
import java.util.Random;

public class main {
    public static void main(String[] args){

        ArrayList<String> nomi = setName();

        int n = 10;
        int g = 7;
        int qmin = 2;
        int qmax = 4;
        int childrenMax = 8;
        int childrenMin = 2;
        ArrayList<Poligono> poligoni = new ArrayList<>();
        Random random = new Random();
        for(int i = 0 ; i < n ; i++){
            Quadrato quadrato = new Quadrato(qmin +random.nextInt(qmax),nomi.get(random.nextInt(nomi.size())));
            poligoni.add(quadrato);
        }

        for(int i = 0 ; i < g ; i++){
            for(int j = 0 ; j <(random.nextInt(childrenMax)+childrenMin); j++ ) {
                Poligono poligono1 = poligoni.get(random.nextInt(poligoni.size()));
                Poligono poligono2 = poligoni.get(random.nextInt(poligoni.size()));
                Rettangolo rettangolo;
                if (random.nextBoolean())
                    rettangolo = new Rettangolo(poligono1.getBase() + poligono2.getBase(), poligono2.getAltezza(), nomi.get(random.nextInt(nomi.size())));
                else
                    rettangolo = new Rettangolo(poligono1.getBase(), poligono1.getAltezza() + poligono2.getAltezza(), nomi.get(random.nextInt(nomi.size())));
                System.out.println("giorno : " + i + " , nome : " + rettangolo.getName() + " , ("+ rettangolo.getBase()+","+ rettangolo.getAltezza() + ") , area : " + rettangolo.getArea());
                poligoni.add(rettangolo);
            }
        }
    }

    public static ArrayList<String> setName(){
        ArrayList<String> list = new ArrayList<>();
        list.add("pietro");
        list.add("luigi");
        list.add("carlo");
        list.add("francesco");
        list.add("giorgio");
        return list;
    }
}
