package packagePaese;

public class Constat {
    public final static String STR_FAMALE = "Femmina";
    public final static String STR_MALE = "Maschio";

    public final static String STR_RECTANGLE = "rettangolo";
    public final static String STR_SQUARE = "quadrato";
    public final static String STR_TRIANGLE = "triangolo";

    public final static String STR_SIG = "Sig.";
    public final static String STR_SIGRA = "Sig.ra";


    public final static String[] ARRAY_MALE_NAME = {"luigi","carlo", "andrea", "francesco"};
    public final static String[] ARRAY_FEMALE_NAME = {"carla","luigia", "alberta", "sara"};

    public final static int CHILDREN_MIN = 2;
    public final static int CHILDREN_MAX = 8;

    public final static int Q_MIN = 2;
    public final static int Q_MAX = 4;

    public final static int NUMBER_POLYGONS_START = 10;
    public final static int NUMBER_OF_DAY = 7;
}
