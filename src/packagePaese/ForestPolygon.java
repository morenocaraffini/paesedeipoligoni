package packagePaese;

import java.util.ArrayList;

public class ForestPolygon {
    private int n = 0;
    private ArrayList<Polygon> polygonArrayList;

    public ForestPolygon(int n ){
        this.n = n;
        polygonArrayList = new ArrayList<>();
    }

    public ArrayList<Polygon> createStartForest(PolygonFactory polygonFactory){

        for(int i = 0 ; i < n ; i++){
            int base = Utils.getRandomInRange(Constat.Q_MIN, Constat.Q_MAX);
            polygonArrayList.add(polygonFactory.getPolygon(PolygonType.Square,base,base));
        }

        return polygonArrayList;
    }
}
