package packagePaese;

public interface Polygon {
    int getBase();
    int getHeight();
    int getArea();
    PolygonType getType();
    String getName();
    String getSex();

}
