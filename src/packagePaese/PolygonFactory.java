package packagePaese;

public class PolygonFactory {
    public PolygonFactory(){
    }

    public Polygon getPolygon(PolygonType type, int base, int height){
        Polygon polygon = null;
        switch (type){
            case Rectangle:
                polygon = new Rectangle(base,height);
                break;
            case Triangle:
                polygon = new Triangle(base,height);
                break;
            case Square:
                polygon = new Square(base);
                break;
        }
        return polygon;
    }
}
