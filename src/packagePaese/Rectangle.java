package packagePaese;

public class Rectangle implements Polygon{
    private int base;
    private int height;
    private String name;
    private String sex;
    private PolygonType type;

    public Rectangle(int base, int height){
        this.base = base;
        this.height = height;
        this.sex = Utils.getRandomSex();
        this.name = Utils.getRandomName(sex, Constat.STR_RECTANGLE);
        this.type = PolygonType.Rectangle;
    }


    @Override
    public int getBase() {
        return base;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getArea() {
        return base * height;
    }

    @Override
    public PolygonType getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSex() {
        return sex;
    }
}
