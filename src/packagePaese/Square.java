package packagePaese;

public class Square implements Polygon{
    private int base;
    private String name;
    private String sex;
    private PolygonType type;

    public Square(int base){
        this.base = base;
        this.sex = Utils.getRandomSex();
        this.name = Utils.getRandomName(sex, Constat.STR_SQUARE);
        this.type = PolygonType.Square;
    }


    @Override
    public int getBase() {
        return base;
    }

    @Override
    public int getHeight() {
        return base;
    }

    @Override
    public int getArea() {
        return base * base;
    }

    @Override
    public PolygonType getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSex() {
        return sex;
    }
}
