package packagePaese.Test;

import org.junit.Assert;
import org.junit.Test;
import packagePaese.ForestPolygon;
import packagePaese.Polygon;
import packagePaese.PolygonFactory;
import packagePaese.PolygonType;

import java.util.ArrayList;

public class ForestPolygonTest {
    @Test
    public void forestPolygonTest(){
        int number_polygon = 7;
        PolygonFactory polygonFactory = new PolygonFactory();
        ForestPolygon forestPolygon = new ForestPolygon(number_polygon);
        ArrayList<Polygon> listPolygon = forestPolygon.createStartForest(polygonFactory);
        Assert.assertEquals(number_polygon, listPolygon.size());
        for(int i = 0 ; i < number_polygon ; i++){
            Assert.assertEquals(PolygonType.Square, listPolygon.get(i).getType());
        }
    }
}
