package packagePaese.Test;

import org.junit.Assert;
import org.junit.Test;
import packagePaese.Polygon;
import packagePaese.PolygonFactory;
import packagePaese.PolygonType;

public class PolygonFactoryTest {
    @Test
    public void polygonFactoryTestCreateRectangle(){
        int base = 2;
        int height = 4;
        PolygonFactory polygonFactory = new PolygonFactory();
        Polygon polygon = polygonFactory.getPolygon(PolygonType.Rectangle,base,height);

        Assert.assertEquals(PolygonType.Rectangle,polygon.getType());
        Assert.assertEquals(base, polygon.getBase());
        Assert.assertEquals(height, polygon.getHeight());
        Assert.assertEquals((base*height), polygon.getArea());
    }

    @Test
    public void polygonFactoryTestCreateSquare(){
        int base = 2;
        PolygonFactory polygonFactory = new PolygonFactory();
        Polygon polygon = polygonFactory.getPolygon(PolygonType.Square,base,base);

        Assert.assertEquals(PolygonType.Square,polygon.getType());
        Assert.assertEquals(base, polygon.getBase());
        Assert.assertEquals(base, polygon.getHeight());
        Assert.assertEquals((base*base), polygon.getArea());
    }

    @Test
    public void polygonFactoryTestCreateTriangle(){
        int base = 2;
        int height = 4;
        PolygonFactory polygonFactory = new PolygonFactory();
        Polygon polygon = polygonFactory.getPolygon(PolygonType.Triangle,base,height);

        Assert.assertEquals(PolygonType.Triangle,polygon.getType());
        Assert.assertEquals(base, polygon.getBase());
        Assert.assertEquals(height, polygon.getHeight());
        Assert.assertEquals(((base*height)/2), polygon.getArea());
    }
}
