package packagePaese.Test;

import org.junit.Assert;
import org.junit.Test;
import packagePaese.PolygonType;
import packagePaese.Utils;

public class UtilsTests {
    @Test
    public void testReproduceTypeSquare(){
        Assert.assertEquals(PolygonType.Rectangle, Utils.getPolygonTypeReproduce(PolygonType.Square,PolygonType.Square));
    }

    @Test
    public void testReproduceTypeRectangle(){
        Assert.assertEquals(PolygonType.Square, Utils.getPolygonTypeReproduce(PolygonType.Rectangle,PolygonType.Rectangle));
    }

    @Test
    public void testReproduceTypeRectangle_2(){
        Assert.assertEquals(PolygonType.Triangle, Utils.getPolygonTypeReproduce(PolygonType.Rectangle,PolygonType.Square));
        Assert.assertEquals(PolygonType.Triangle, Utils.getPolygonTypeReproduce(PolygonType.Square,PolygonType.Rectangle));
    }

    @Test
    public void testReproduceTypeTriangle(){
        Assert.assertEquals(PolygonType.Rectangle, Utils.getPolygonTypeReproduce(PolygonType.Triangle,PolygonType.Triangle));
    }

    @Test
    public void testReproduceImpossible(){
        Assert.assertEquals(null, Utils.getPolygonTypeReproduce(PolygonType.Triangle,PolygonType.Square));
        Assert.assertEquals(null, Utils.getPolygonTypeReproduce(PolygonType.Triangle,PolygonType.Rectangle));
        Assert.assertEquals(null, Utils.getPolygonTypeReproduce(PolygonType.Square,PolygonType.Triangle));
        Assert.assertEquals(null, Utils.getPolygonTypeReproduce(PolygonType.Rectangle,PolygonType.Triangle));
    }


}
