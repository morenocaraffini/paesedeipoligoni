package packagePaese;

public class Triangle implements Polygon {
    private int base;
    private int height;
    private String name;
    private String sex;
    private PolygonType type;

    public Triangle(int base, int height){
        this.base = base;
        this.height = height;
        this.sex = Utils.getRandomSex();
        this.name = Utils.getRandomName(sex, Constat.STR_TRIANGLE);
        this.type = PolygonType.Triangle;
    }


    @Override
    public int getBase() {
        return base;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getArea() {
        return (base * height)/2;
    }

    @Override
    public PolygonType getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSex() {
        return sex;
    }

}
