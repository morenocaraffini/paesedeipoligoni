package packagePaese;

import java.util.Random;

public class Utils {

    public static int getRandomInRange(int a, int b ){
        Random random = new Random();
        return a + random.nextInt(b);
    }

    public static int getRandomInRange(int a){
        Random random = new Random();
        return random.nextInt(a);
    }

    public static String getRandomSex(){
        Random random = new Random();
        if(random.nextBoolean())
            return Constat.STR_FAMALE;
        else
            return Constat.STR_MALE;
    }

    public static String getRandomName(String sex, String type){
        Random random = new Random();
        if(sex.equals(Constat.STR_FAMALE)) {
            String name = (Constat.ARRAY_FEMALE_NAME[random.nextInt(Constat.ARRAY_FEMALE_NAME.length)]+ "_" + type).toUpperCase();
            return Constat.STR_SIGRA + " " + name;
        }else{
            String name = (Constat.ARRAY_MALE_NAME[random.nextInt(Constat.ARRAY_MALE_NAME.length)]+ "_" + type).toUpperCase();
            return Constat.STR_SIG + " " +name;

        }
    }

    public static PolygonType getPolygonTypeReproduce(PolygonType p1 , PolygonType p2){
        if(p1.equals(PolygonType.Square) && p2.equals(PolygonType.Square)){
            return PolygonType.Rectangle;
        }else if(p1.equals(PolygonType.Rectangle) && p2.equals(PolygonType.Rectangle)){
            return PolygonType.Square;
        }else if(p1.equals(PolygonType.Square) && p2.equals(PolygonType.Rectangle)||p1.equals(PolygonType.Rectangle) && p2.equals(PolygonType.Square)){
            return PolygonType.Triangle;
        }else if(p1.equals(PolygonType.Triangle) && p2.equals(PolygonType.Triangle)){
            return PolygonType.Rectangle;
        }else{
            return null;
        }
    }


    public static Polygon createNewPolygon(PolygonType type,PolygonFactory polygonFactory, Polygon p1, Polygon p2){
        int baseNew = 0;
        int heightNew = 0;
        Random random = new Random();
        if(random.nextBoolean()){
            baseNew = p1.getBase()+p2.getBase();
            heightNew = p1.getHeight();
        }else {
            baseNew = p1.getBase();
            heightNew = p1.getHeight()+p2.getHeight();
        }
        return  polygonFactory.getPolygon(type,baseNew,heightNew);
    }
}
