package packagePaese;

import java.util.ArrayList;

public class main {

    public static void main(String[] args){

        PolygonFactory polygonFactory = new PolygonFactory();
        ForestPolygon forestPolygon = new ForestPolygon(Constat.NUMBER_POLYGONS_START);
        ArrayList<Polygon> polygonsForestList = forestPolygon.createStartForest(polygonFactory);

        for(int i = 1; i <= Constat.NUMBER_OF_DAY ; i++){
            for(int j = 0 ;j < Utils.getRandomInRange(Constat.CHILDREN_MIN,Constat.CHILDREN_MAX);j++){
                Polygon polygon1 = polygonsForestList.get(Utils.getRandomInRange(polygonsForestList.size()));
                Polygon polygon2 = polygonsForestList.get(Utils.getRandomInRange(polygonsForestList.size()));
                PolygonType newPolygonType = Utils.getPolygonTypeReproduce(polygon1.getType(),polygon2.getType());
                if(newPolygonType != null){
                    Polygon polygon = Utils.createNewPolygon(newPolygonType,polygonFactory,polygon1,polygon2);
                    System.out.println("giorno : " + i + " , nome : " +  polygon.getName() + " , area : " + polygon.getArea());
                    polygonsForestList.add(polygon);
                }
            }
        }
    }
}
